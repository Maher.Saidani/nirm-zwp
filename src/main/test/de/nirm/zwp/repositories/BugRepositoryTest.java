package de.nirm.zwp.repositories;


import de.nirm.zwp.entities.Bug;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class BugRepositoryTest {

    BugRepository bugRepository;
    Bug testBug;

    @Before
    public void init(){

        //h2 in memory (datenbank im Speicher) - Integrationstest
        bugRepository = new BugRepository();
        bugRepository.createEntityManager(); //@PostConstructor funktionniert nicht im Test

        testBug = new Bug();
        testBug.setName("Testbug");
        testBug.setDescription("This Bug is created for a test use");
        testBug.setReqDate(ZonedDateTime.now());
        testBug.setStatus(Boolean.FALSE);
        testBug.setPriority("MEDIUM");

        bugRepository.writeBug(testBug);
    }


    @After
    public void finalSet(){
        if (bugRepository.readBugs().contains(testBug)){
            bugRepository.deleteBug(testBug);
        }
    }


    @Test
    public void writeBugTest(){
        assertEquals(testBug,bugRepository.readBug(testBug.getId()));
    }

    @Test
    public void readBugTest(){

        Bug expectedBug = bugRepository.readBug(testBug.getId());
        assertEquals(expectedBug,testBug);

    }

    @Test
    public void readBugsTest(){

        List<Bug> bugs = bugRepository.readBugs();
        assertNotNull(bugs);
        assertTrue(bugs.size()>=1);

    }


    @Test
    public void deleteBugTest(){

        bugRepository.deleteBug(testBug);
        assertFalse(bugRepository.readBugs().contains(testBug));

    }

}
