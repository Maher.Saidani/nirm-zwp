package de.nirm.zwp.repositories;


import de.nirm.zwp.entities.Bug;
import de.nirm.zwp.entities.Developer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class DevRepositoryTest {

    DevRepository devRepository;
    Developer testDev;

    @Before
    public void init(){
        //h2 in memory (datenbank im Speicher) - Integrationstest
        devRepository = new DevRepository();
        devRepository.createEntityManager(); //@PostConstructor funktionniert nicht im Test
        testDev = new Developer();
        testDev.setName("TestDev");

        devRepository.writeDeveloper(testDev);
    }


    @After
    public void finalSet(){
        if (devRepository.readDevelopers().contains(testDev)){
            devRepository.deleteDeveloper(testDev.getId());
        }
    }


    @Test
    public void writeDevTest(){
        assertEquals(testDev, devRepository.readDeveloper(testDev.getId()));
    }

    @Test
    public void readDevTest(){
        Developer expectedDev = devRepository.readDeveloper(testDev.getId());
        assertEquals(expectedDev, testDev);
    }

    @Test
    public void readDevsTest(){
        List<Developer> devs = devRepository.readDevelopers();
        assertNotNull(devs);
        assertTrue(devs.size()>=1);
    }


    @Test
    public void deleteDevTest(){
        devRepository.deleteDeveloper(testDev.getId());
        assertFalse(devRepository.readDevelopers().contains(testDev));

    }

}
