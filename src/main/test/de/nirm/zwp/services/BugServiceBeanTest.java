package de.nirm.zwp.services;


import de.nirm.zwp.entities.Bug;
import de.nirm.zwp.models.BugModel;
import de.nirm.zwp.repositories.BugRepository;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BugServiceBeanTest {

    BugServiceBean bugServiceBean;

    @Before
    public void init(){
        bugServiceBean = new BugServiceBean();
    }


    @Test
    public void mapToEntityTest(){

        BugModel testBugModel = new BugModel();
        testBugModel.setBugName("testBug");
        testBugModel.setBugDescription("This Bug is created for a test use");
        testBugModel.setBugReqDate(ZonedDateTime.now());
        testBugModel.setBugStatus(Boolean.FALSE);
        testBugModel.setPriority("MEDIUM");

        Bug testBug = bugServiceBean.mapToEntity(testBugModel);

        assertTrue(testBug.getName()==testBugModel.getBugName());
        assertTrue(testBug.getId()==testBugModel.getBugId());
        assertTrue(testBug.getPriority()==testBugModel.getPriority());
        assertTrue(testBug.getDescription()==testBugModel.getBugDescription());
        assertTrue(testBug.getStatus()==testBugModel.getBugStatus());
        assertTrue(testBug.getReqDate()==testBugModel.getBugReqDate());

    }

    @Test
    public void mapToModelTest(){

        Bug testBug = new Bug();
        testBug.setName("Testbug");
        testBug.setDescription("This Bug is created for a test use");
        testBug.setReqDate(ZonedDateTime.now());
        testBug.setStatus(Boolean.FALSE);
        testBug.setPriority("MEDIUM");

        BugModel testBugModel = bugServiceBean.mapToModel(testBug);

        assertTrue(testBug.getName()==testBugModel.getBugName());
        assertTrue(testBug.getId()==testBugModel.getBugId());
        assertTrue(testBug.getPriority()==testBugModel.getPriority());
        assertTrue(testBug.getDescription()==testBugModel.getBugDescription());
        assertTrue(testBug.getStatus()==testBugModel.getBugStatus());
        assertTrue(testBug.getReqDate()==testBugModel.getBugReqDate());

    }

}
