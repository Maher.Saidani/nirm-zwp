package de.nirm.zwp.services;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class NotificationServiceBeanTest {

    NotificationServiceBean notificationServiceBean;

    @Before
    public void init(){
        notificationServiceBean = new NotificationServiceBean();

        notificationServiceBean.addGoodNotification("Entry1");
        notificationServiceBean.addGoodNotification("Entry2");

        notificationServiceBean.addNeutralNotification("Entry1");
        notificationServiceBean.addNeutralNotification("Entry2");

        notificationServiceBean.addBadNotification("Entry1");
        notificationServiceBean.addBadNotification("Entry2");
    }

    @Test
    public void goodNotificationTest() {
        List<String> notifications = notificationServiceBean.getGoodNotificationList();
        assertEquals(2,notifications.size());

        assertEquals("Entry1",notifications.get(0));
        assertEquals("Entry2",notifications.get(1));

        notifications = notificationServiceBean.getGoodNotificationList();
        assertEquals(0,notifications.size());
    }
    @Test
    public void neutralNotificationTest() {
        List<String> notifications = notificationServiceBean.getNeutralNotificationList();
        assertEquals(2,notifications.size());

        assertEquals("Entry1",notifications.get(0));
        assertEquals("Entry2",notifications.get(1));

        notifications = notificationServiceBean.getNeutralNotificationList();
        assertEquals(0,notifications.size());
    }
    @Test
    public void badNotificationTest() {
        List<String> notifications = notificationServiceBean.getBadNotificationList();
        assertEquals(2,notifications.size());

        assertEquals("Entry1",notifications.get(0));
        assertEquals("Entry2",notifications.get(1));

        notifications = notificationServiceBean.getBadNotificationList();
        assertEquals(0,notifications.size());
    }
}


