package de.nirm.zwp.services;


import de.nirm.zwp.entities.Developer;
import de.nirm.zwp.models.DeveloperModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DevSeviceBeanTest {

    DevServiceBean devServiceBean;
    Developer devEntity;
    DeveloperModel devModel;

    @Before
    public void init(){
        devServiceBean = new DevServiceBean();

        devEntity = new Developer();
        devEntity.setId(1L);
        devEntity.setName("TestName");

        devModel = new DeveloperModel();
        devModel.setDevId(1L);
        devModel.setDevName("TestName");
    }

    @Test
    public void mapToModel(){
        DeveloperModel outputModel = devServiceBean.mapToModel(devEntity);

        assertEquals(outputModel.getDevId(), devEntity.getId());
        assertEquals(outputModel.getDevName(), devEntity.getName());
    }

    @Test
    public void mapToEntity(){
        Developer outputEntity = devServiceBean.mapToEntity(devModel);

        assertEquals(outputEntity.getId(), devModel.getDevId());
        assertEquals(outputEntity.getName(), devModel.getDevName());
    }


}
