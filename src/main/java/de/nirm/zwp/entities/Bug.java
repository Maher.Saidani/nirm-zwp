package de.nirm.zwp.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
public class Bug {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column
    @Lob
    @NotNull
    private String description;

    @Column
    private Boolean status;

    @Column
    private ZonedDateTime reqDate;

    @Column
    private ZonedDateTime fixDate;

    @Column
    private String priority;

    @OneToOne
    @JoinColumn()
    private Developer developer;

    public Bug(){}

    @Override
    public String toString() {
        return "Bug{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    //Getter - Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ZonedDateTime getReqDate() {
        return reqDate;
    }

    public void setReqDate(ZonedDateTime reqDate) {
        this.reqDate = reqDate;
    }

    public ZonedDateTime getFixDate() {
        return fixDate;
    }

    public void setFixDate(ZonedDateTime fixDate) {
        this.fixDate = fixDate;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
