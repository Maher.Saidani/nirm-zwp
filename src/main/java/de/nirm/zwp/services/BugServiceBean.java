package de.nirm.zwp.services;


import de.nirm.zwp.entities.Bug;
import de.nirm.zwp.entities.Developer;
import de.nirm.zwp.models.BugModel;
import de.nirm.zwp.models.DeveloperModel;
import de.nirm.zwp.repositories.BugRepository;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.Query;
import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.List;

/**
 * Mapping zwischen Entity- und Model-Bugs
 * */
@Stateful
public class BugServiceBean {

    @EJB
    BugRepository bugRepository;

    @EJB
    DevServiceBean devServiceBean;

    public BugServiceBean() {
    }


    /**
     * Bug-Entities auslesen und in Bug-Models mappen
     * @return Liste der BugModels aus der Bug-Entites
     * */
    public List<BugModel> readBugs(){

        List<BugModel> bugModels = new ArrayList<>();
        List<Bug> bugs = bugRepository.readBugs();

        bugs.forEach(bug -> {
            BugModel bugModel = mapToModel(bug);
            if (bug.getDeveloper() != null){
                bugModel.setDeveloper(devServiceBean.mapToModel(bug.getDeveloper()));
            }
            bugModels.add(bugModel);
        });

        return bugModels;
    }

    /**
     * Mapping BugEntity to Bugmodel
     * @return Bug-Model
     * */
    public BugModel mapToModel(Bug bug){

        BugModel bugModel = new BugModel();

        bugModel.setBugId(bug.getId());
        bugModel.setBugName(bug.getName());
        bugModel.setBugDescription(bug.getDescription());
        bugModel.setBugReqDate(bug.getReqDate());
        bugModel.setBugStatus(bug.getStatus());
        bugModel.setFixDate(bug.getFixDate());
        bugModel.setPriority(bug.getPriority());

        return bugModel;
    }


    /**
     * Mapping BugModel to BugEntity
     * @return Bug-Model
     * */
    public Bug mapToEntity(BugModel bugModel){

        Bug bug = new Bug();

        bug.setId(bugModel.getBugId());
        bug.setName(bugModel.getBugName());
        bug.setDescription(bugModel.getBugDescription());
        bug.setReqDate(bugModel.getBugReqDate());
        bug.setStatus(bugModel.isBugStatus());
        bug.setFixDate(bugModel.getFixDate());
        bug.setPriority(bugModel.getPriority());

        return bug;
    }

    /**
     * BugModel in BugEntity mappen
     * */
    public void writeBug(BugModel bugModel){

        System.out.println("Model  write : " + bugModel.getPriority());

        Bug bug = mapToEntity(bugModel);

        bugRepository.writeBug(bug);
    }


    /**
     * Open BugModels filtern
     * @return Liste der BugModels mit Status Open
     **/
    public List<BugModel> readOpenBugs(){
        List<BugModel> openBugs = new ArrayList<>();
        List<BugModel> bugModels = readBugs();

        bugModels.forEach(bugModel -> {
            if (bugModel.isBugStatus()==Boolean.TRUE && bugModel.getDeveloper() == null){
                openBugs.add(bugModel);
            }
        });
        return openBugs;
    }

    public List<BugModel> readAssignedBugs() {
        List<BugModel> assignedBugs = new ArrayList<>();
        List<BugModel> bugModels = readBugs();

        bugModels.forEach(bugModel -> {
            if (bugModel.isBugStatus()==Boolean.TRUE && bugModel.getDeveloper() != null){
                assignedBugs.add(bugModel);
            }
        });

        return assignedBugs;
    }


    public void reserve(BugModel bugModel, DeveloperModel developerModel){

        Developer developer = devServiceBean.mapToEntity(developerModel);

        Bug bug = bugRepository.readBug(bugModel.getBugId());
        bug.setDeveloper(developer);
        bugRepository.writeBug(bug);
    }

    public void reopenBug(BugModel bugModel) {
        Bug bugEntity = bugRepository.readBug(bugModel.getBugId());
        bugEntity.setDeveloper(null);
        bugRepository.writeBug(bugEntity);
    }

    public void closeBug(BugModel bugModel) {
        Bug bugEntity = bugRepository.readBug(bugModel.getBugId());
        bugEntity.setStatus(bugModel.isBugStatus());
        bugEntity.setFixDate(bugModel.getFixDate());
        bugRepository.writeBug(bugEntity);
    }

    /**
     * Closes BugModels filtern
     * @return Liste der BugModels mit Status Colsed
     **/
    public List<BugModel> readClosedBugs(){

        List<BugModel> closedBugs = new ArrayList<>();

        List<BugModel> bugModels = readBugs();

        bugModels.forEach(bugModel -> {
            if (bugModel.isBugStatus()==Boolean.FALSE){
                closedBugs.add(bugModel);
            }
        });

        return closedBugs;
    }

    public BugModel readBug(Long id){
        Bug bug = bugRepository.readBug(id);
        BugModel bugmodel = new BugModel();

        bugmodel.setBugName(bug.getName());
        bugmodel.setBugDescription(bug.getDescription());
        bugmodel.setPriority(bug.getPriority());
//        bugmodel.setDeveloper(bug.getDeveloper());
        bugmodel.setBugReqDate(bug.getReqDate());
//        if(bug.getFixDate() !=null){
//            bugmodel.setFixDate(bug.setFixDate());
//        }

        return bugmodel;
    }


    public void updatePriority(BugModel bugModel, String priority) {
        Bug bug = bugRepository.readBug(bugModel.getBugId());
        bug.setPriority(priority);
        bugRepository.writeBug(bug);
    }
}
