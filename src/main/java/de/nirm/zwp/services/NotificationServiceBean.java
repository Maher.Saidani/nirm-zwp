package de.nirm.zwp.services;

import javax.ejb.Stateless;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class takes Notifications that you want to output to the User in he Browser
 * The next reload of the Webpage will display these Messages as an Alert in 3 different Styles
 */
@Stateless
public class NotificationServiceBean {
    /**
     * Messages that yoy want to Display Bootstrap-success Style
     */
    private List<String> goodNotificationList = new ArrayList<>();
    /**
     * Messages that yoy want to Display Bootstrap-primary Style
     */
    private List<String> neutralNotificationList = new ArrayList<>();
    /**
     * Messages that yoy want to Display Bootstrap-danger Style
     */
    private List<String> badNotificationList = new ArrayList<>();

    public void addGoodNotification(String message) {
        goodNotificationList.add(message);
    }
    public void addNeutralNotification(String message) {
        neutralNotificationList.add(message);
    }
    public void addBadNotification(String message) {
        badNotificationList.add(message);
    }

    public List<String> getGoodNotificationList(){
        List<String> messages = new ArrayList<>(goodNotificationList);
        //Messages are deleted after you read them. So you only display them once
        goodNotificationList = new ArrayList<>();
        return messages;
    }

    public List<String> getNeutralNotificationList(){
        List<String> messages = new ArrayList<>(neutralNotificationList);
        //Messages are deleted after you read them. So you only display them once
        neutralNotificationList = new ArrayList<>();
        return messages;
    }

    public List<String> getBadNotificationList(){
        List<String> messages = new ArrayList<>(badNotificationList);
        //Messages are deleted after you read them. So you only display them once
        badNotificationList = new ArrayList<>();
        return messages;
    }
}
