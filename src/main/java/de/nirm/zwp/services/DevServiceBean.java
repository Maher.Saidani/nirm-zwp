package de.nirm.zwp.services;


import de.nirm.zwp.entities.Bug;
import de.nirm.zwp.entities.Developer;
import de.nirm.zwp.models.DeveloperModel;
import de.nirm.zwp.repositories.BugRepository;
import de.nirm.zwp.repositories.DevRepository;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import java.util.ArrayList;
import java.util.List;


@Stateful
public class DevServiceBean {

    @EJB
    DevRepository devRepository;

    @EJB
    BugRepository bugRepository;

    @EJB
    NotificationServiceBean notificationServiceBean;


    public DevServiceBean() {
    }

    /**
     * Gets the List of Developer-Entities from DB and Maps them to Developer-Models
     * @return List of DeeloperModels
     */
    public List<DeveloperModel> readDevelopers(){

        List<DeveloperModel> devModels = new ArrayList<>();
        List<Developer> devs = devRepository.readDevelopers();

        devs.forEach(dev -> {
            // Mapping devModel <- Deveoper
            DeveloperModel devModel = mapToModel(dev);
            devModels.add(devModel);
        });
        return devModels;
    }


    /**
     * Mapping from DevEntity to DevModel
     * @param dev The Entity of the Developer
     * @return the Model of the Developer
     */
    public DeveloperModel mapToModel(Developer dev) {
        DeveloperModel devModel = new DeveloperModel();
        if (dev!=null){
            devModel.setDevId(dev.getId());
            devModel.setDevName(dev.getName());
        }
        return devModel;
    }

    /**
     * Writes a new Developer the DB. Checks if name is unique and gives Feedback to the user
     * @param devModel Developer Model to write To DB
     */
    public void writeDeveloper(DeveloperModel devModel){
        boolean unique = true;
        for(DeveloperModel dev : readDevelopers()) {
            if (dev.getDevName().trim().toLowerCase().equals(devModel.getDevName().toLowerCase()) ) {
                unique = false;
            }
        }
        if (unique){
            Developer dev = mapToEntity(devModel);
            devRepository.writeDeveloper(dev);
            notificationServiceBean.addGoodNotification("New Developer '"+devModel.getDevName()+"' has been created!");
        } else {
            notificationServiceBean.addBadNotification("There is already a Developer called " + devModel.getDevName());
        }
    }
    /**
     * Mapping from DevModel to DevEntity
     * @param devModel The odel of the Developer
     * @return the Entity of the Developer
     */
    public Developer mapToEntity(DeveloperModel devModel) {
        Developer dev = new Developer();
        // Mapping bug <- bugModel
            if (devModel != null){
                dev.setId(devModel.getDevId());
                dev.setName(devModel.getDevName());
            }

        return dev;
    }

    /**
     * Deletes a Developer from DB
     * @param id The ID of the Developer you want to delete
     */
    public void deleteDeveloper(Long id) {

        List<Bug> bugs = bugRepository.findBugOfDev(id);
        bugs.forEach(bug -> {
            bug.setDeveloper(null);
            bugRepository.writeBug(bug);
        });

        devRepository.deleteDeveloper(id);
        notificationServiceBean.addGoodNotification("A Developer has been deleted!");

    }

}
