package de.nirm.zwp.controllers;

import de.nirm.zwp.models.BugModel;
import de.nirm.zwp.models.DeveloperModel;
import de.nirm.zwp.services.BugServiceBean;
import de.nirm.zwp.services.NotificationServiceBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class StartBean {
    private final HashMap<String, String> PRIORITIES = new HashMap<String, String>(){
        {
            put("High", "1HIGH");
            put("Medium", "2MEDIUM");
            put("Low", "3LOW");
        }
    };

    @EJB
    private BugServiceBean bugServiceBean;

    public List<BugModel> getAllBugs() {
        return bugServiceBean.readBugs();
    }

    public List<BugModel> getOpenBugs() {
        return bugServiceBean.readOpenBugs();
    }

    public List<BugModel> getAssignedBugs() {
        return bugServiceBean.readAssignedBugs();
    }

    public List<BugModel> getClosedBugs() {
        return bugServiceBean.readClosedBugs();
    }

    public HashMap<String, String> getPRIORITIES() {
        return PRIORITIES;
    }

    public List<String> getPriorityLabel() {
        return new ArrayList<>(PRIORITIES.keySet());
    }

    public String getPriorityCSS(String priorityValue){
        for (Map.Entry<String, String> entry : PRIORITIES.entrySet()) {
            if (entry.getValue().equals(priorityValue)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void reserve(BugModel bugModel, DeveloperModel developerModel) {
        bugServiceBean.reserve(bugModel, developerModel);
    }

    public void updatePriority(BugModel bugModel, String prioritylabel){
        String priority = PRIORITIES.get(prioritylabel);
        bugServiceBean.updatePriority(bugModel, priority);
    }

    /**
     * When you Fixed a Bug you can close it. It will be displayed under the closed Tag
     * @param bugModel Bug Model that you want to close
     */
    public void closeBug(BugModel bugModel){
        bugModel.setBugStatus(false);
        bugModel.setFixDateToNow();
        bugServiceBean.closeBug(bugModel);
    }
    /**
     * When you can not fix a Bug you can set it to be open for everyone again.
     * @param bugModel Bug Model that you want re-open
     */
    public void reopenBug(BugModel bugModel){
        bugModel.setDeveloper(null);
        bugServiceBean.reopenBug(bugModel);
    }

}
