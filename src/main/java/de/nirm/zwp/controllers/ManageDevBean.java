package de.nirm.zwp.controllers;

import de.nirm.zwp.models.DeveloperModel;
import de.nirm.zwp.services.DevServiceBean;
import de.nirm.zwp.services.NotificationServiceBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class ManageDevBean {

    @EJB
    private DevServiceBean devServiceBean;

    private DeveloperModel newDevModel;

    @PostConstruct
    private void init(){
        newDevModel = new DeveloperModel();
    }

    public void submitDev(){
        devServiceBean.writeDeveloper(newDevModel);
        newDevModel.setDevName("");
    }

    public void deleteDev(Long id){
        devServiceBean.deleteDeveloper(id);
    }

    public List<DeveloperModel> getDevelopers(){
        return devServiceBean.readDevelopers();
    }

    public DeveloperModel getNewDevModel() {
        return newDevModel;
    }

    public void setNewDevModel(DeveloperModel newDevModel) {
        this.newDevModel = newDevModel;
    }
}
