package de.nirm.zwp.controllers;

import de.nirm.zwp.models.BugModel;
import de.nirm.zwp.services.BugServiceBean;
import de.nirm.zwp.services.NotificationServiceBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;


@Named
@RequestScoped
public class CreateBugBean {
    @EJB
    private BugServiceBean bugServiceBean;
    @EJB
    private NotificationServiceBean notificationServiceBean;

    private BugModel newBugModel;

    @PostConstruct
    private void init(){
        newBugModel = new BugModel();
        newBugModel.setPriority("2MEDIUM");
    }

    /**
     * Submits a Bug and saves it with status open into the DB. and redirects to the startpage
     * @return return the a String with the page Name that you want to redirect to
     */
    public String submitBug(){
        newBugModel.setBugReqDate(ZonedDateTime.now(ZoneId.of("Europe/Berlin")));
        bugServiceBean.writeBug(newBugModel);
        notificationServiceBean.addGoodNotification("Your Bug '"+newBugModel.getBugName()+"' has been created!");
        return "start";
    }

    public void setBugs(List<BugModel> allbugs) {

    }

    public BugModel getNewBugModel() {
        return newBugModel;
    }

    public void setNewBugModel(BugModel newBugModel) {
        this.newBugModel = newBugModel;
    }
}
