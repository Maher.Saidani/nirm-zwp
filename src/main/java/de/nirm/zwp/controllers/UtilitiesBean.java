package de.nirm.zwp.controllers;

import de.nirm.zwp.services.NotificationServiceBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Named
@RequestScoped
public class UtilitiesBean {

    @EJB
    private NotificationServiceBean notificationServiceBean;

    /**
     * Formats a Date in a Standart Way for our Program
     * @param dateTime DateObject that you want to format
     * @return String in Form of the pattern "dd/MM/yyyy - HH:mm:ss"
     */
    public String formatDate(ZonedDateTime dateTime){
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm:ss");
        return dateTime.format(formatter2);
    }


    public List<String> getGoodNotifications() {
        return notificationServiceBean.getGoodNotificationList();
    }

    public List<String> getNeutralNotifications() {
        return notificationServiceBean.getNeutralNotificationList();
    }

    public List<String> getBadNotifications() {
        return notificationServiceBean.getBadNotificationList();
    }
}
