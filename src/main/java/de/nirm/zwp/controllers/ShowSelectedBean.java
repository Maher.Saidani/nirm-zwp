package de.nirm.zwp.controllers;

import de.nirm.zwp.models.BugModel;
import de.nirm.zwp.services.BugServiceBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class ShowSelectedBean {
    @EJB
    private BugServiceBean bugServiceBean;

    private BugModel bugModel;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        bugModel = bugServiceBean.readBug(id);
    }

    public BugModel readBug(Long id){
        return bugServiceBean.readBug(id);
    }

    public BugModel getBugModel() {
        return bugModel;
    }

}
