package de.nirm.zwp.repositories;

import de.nirm.zwp.entities.Bug;
import org.w3c.dom.ls.LSOutput;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


/**
 * Queries zum Bug Schreiben/Lesen über Entity-Manager erstellen
 * */
@Stateless
public class BugRepository {
    private static EntityManager entityManager;
    public BugRepository() {
    }


    @PostConstruct
    public void createEntityManager(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory
                ("JPA_BugFix_PERSISTENCE_UNIT");
        entityManager = entityManagerFactory.createEntityManager();
    }


    /**
     * Alle Bugs aus der Tabelle Bug in der Datenbank auslesen
     * @return Eine Liste von Bug-Entitäten
     * */
    public List<Bug> readBugs(){

        Query query = entityManager.createQuery("SELECT b from Bug as b ORDER BY b.priority, b.reqDate desc", Bug.class);

        List<Bug> bugEntities = query.getResultList();

        return bugEntities;
    }


    /**
     * Ein Bug-Entity aus der Datenbanktabelle auslesen
     **/
    public Bug readBug(Long bugId){

        //Query query = entityManager.createQuery("SELECT b from Bug b where b.id= :bugId");
        Bug bug = entityManager.find(Bug.class,bugId);

        return bug;
    }

    /**
     * Ein Bug in die Datenbanktabelle Bug hinzufügen
     * */
    public void writeBug(Bug bug){

        entityManager.getTransaction().begin();
        entityManager.persist(bug);
        entityManager.getTransaction().commit();

    }



    /**
     * Ein Bug aus der Datenbanktabelle Bug löschen
     * */
    public void deleteBug(Bug bug){

        entityManager.getTransaction().begin();
        entityManager.remove(bug);
        entityManager.getTransaction().commit();

    }

    public List<Bug> findBugOfDev(Long id){


        Query query = BugRepository.getEntityManager().createQuery
                ("SELECT b from Bug b where b.developer.id=:id",Bug.class);

        List<Bug> bugs = query.setParameter("id",id).getResultList();

        return bugs;
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    public void updatePriority(Bug bug) {
        entityManager.getTransaction().begin();
        entityManager.persist(bug);
        entityManager.getTransaction().commit();
    }
}