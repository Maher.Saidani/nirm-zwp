package de.nirm.zwp.repositories;

import de.nirm.zwp.entities.Developer;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class DevRepository {

    private static EntityManager entityManager;

    public DevRepository() {
    }

    @PostConstruct
    public void createEntityManager(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory
                ("JPA_BugFix_PERSISTENCE_UNIT");
        entityManager = entityManagerFactory.createEntityManager();
    }

    /**
     * Resturns a list of all Developer Entities
     * @return List of the Developer Entities
     */
    public List<Developer> readDevelopers(){
        Query query = entityManager.createQuery("SELECT d from Developer d", Developer.class);
        List<Developer> devEntities = query.getResultList();
        return devEntities;
    }


    /**
     * Writes a new Developer to the Database
     * @param dev Developer Entity to write to Database
     */
    public void writeDeveloper(Developer dev){
        entityManager.getTransaction().begin();
        entityManager.persist(dev);
        entityManager.getTransaction().commit();
    }

    /**
     * Deletes a Developer
     * @param id ID of the Developer that you want to be deleted from DB
     */
    public void deleteDeveloper(Long id){
        Developer dev = entityManager.find(Developer.class, id);
        entityManager.getTransaction().begin();
        entityManager.remove(dev);
        entityManager.getTransaction().commit();
    }

    public Developer readDeveloper(Long id) {
        return entityManager.find(Developer.class, id);

    }
}


