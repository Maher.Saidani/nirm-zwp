package de.nirm.zwp.models;

public class DeveloperModel {
    private Long devId;
    private String devName;

    public DeveloperModel() {}

    @Override
    public String toString() {
        return devName;
    }

    public Long getDevId() {
        return devId;
    }

    public void setDevId(Long devId) {
        this.devId = devId;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName.trim();
    }
}
