package de.nirm.zwp.models;

import java.time.ZonedDateTime;

public class BugModel {
    private Long bugId;
    private String bugName;
    private String bugDescription;
    private ZonedDateTime bugReqDate;
    private ZonedDateTime fixDate;
    private Boolean bugStatus;
    private DeveloperModel developer;
    private String priority;




    public BugModel() {
        this.bugReqDate = ZonedDateTime.now();
        this.bugStatus = true;
    }

    public Long getBugId() {
        return bugId;
    }

    public void setBugId(Long bugId) {
        this.bugId = bugId;
    }

    public String getBugName() {
        return bugName;
    }

    public void setBugName(String bugName) {
        this.bugName = bugName;
    }

    public String getBugDescription() {
        return bugDescription;
    }

    public void setBugDescription(String bugDescription) {
        this.bugDescription = bugDescription;
    }

    public ZonedDateTime getBugReqDate() {
        return bugReqDate;
    }

    public void setBugReqDate(ZonedDateTime bugReqDate) {
        this.bugReqDate = bugReqDate;
    }

    public Boolean isBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(Boolean bugStatus) {
        this.bugStatus = bugStatus;
    }

    public DeveloperModel getDeveloper() {
        return developer;
    }

    public void setDeveloper(DeveloperModel developer) {
        this.developer = developer;
    }

    public ZonedDateTime getFixDate() {
        return fixDate;
    }

    public void setFixDate(ZonedDateTime fixDate) {
        this.fixDate = fixDate;
    }
    public void setFixDateToNow() {
        this.fixDate = ZonedDateTime.now();
    }

    public Boolean getBugStatus() {
        return bugStatus;
    }

//    public Priority getPriority() {
//        return priority;
//    }


    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }
}
